//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day10: ObservableObject, RunableProtocol {
    
    struct Person {
        private(set) var firstName: String
        private(set) var lastName: String
        private(set) var age: Int
        var workingEnd: Int {
            get {
                age
            }
            set {
                age = newValue
            }
        }
        
        mutating func setAge(newAge: Int) {
            age = newAge
        }
        
        func toString() {
            print("""
            \(firstName) \(lastName) \(age)
            """)
        }
    }
    
    struct App {
        var contacts = [String]() {
            willSet {
                print("New value will be: \(newValue)")
            }

            didSet {
                print("Old value was \(oldValue)")
            }
        }
    }
    
    struct People {
        private var surname: String
        
        init(surname: String) {
            self.surname = surname
        }
        
        func toString() {
            print("Surname : ",surname)
        }
    }
    
    func execute() {
        var person1 = Person(firstName: "Iqbal", lastName: "Rahman", age: 20)
        var person2 = Person(firstName: "Bambang", lastName: "Sugeni", age: 30)
        
        person1.toString()
        
        person1.setAge(newAge: 21)
        person1.toString()
        print("Batas umur : ",person1.workingEnd)
        person2.toString()
        person2.workingEnd = 32
        print("Batas umur: ",person2.workingEnd)
        
        var app = App()
        app.contacts.append("M. Iqbal Rahman")
        app.contacts.append("Viviana F")
        app.contacts.append("Ansel M")
        
        let people1 = People(surname: "Budiman Yahya")
        people1.toString()
    }
}
