//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day12: ObservableObject, RunableProtocol {
    class Person {
        private var firstName: String
        private var lastName: String
        
        init(firstName: String, lastName: String) {
            self.firstName = firstName
            self.lastName = lastName
        }
        
        func setFirstName(firstName: String) {
            self.firstName = firstName
        }
        
        func toString() {
            print("""
            \(firstName) \(lastName)
            """)
        }
    }
    
    class Teacher: Person {
        private var teachTime: Int
        private var classRoom = [String]()
        
        init(firstName: String, lastName: String, teachTime: Int) {
            self.teachTime = teachTime
            super.init(firstName: firstName, lastName: lastName)
        }
        
        deinit {
            super.toString()
            print(classRoom)
            print("- destroyed ...")
        }
        
        func add(className: String) {
            classRoom.append(className)
        }
        
        override
        func toString() {
            super.toString()
            print("\(teachTime) Hours")
        }
    }
    
    func execute() {
        let teacher1 = Teacher(firstName: "Iqbal", lastName: "Rahman", teachTime: 10)
        teacher1.toString()
        
        let teacher2 = teacher1
        
        teacher2.setFirstName(firstName: "Budiman")
        teacher1.toString()
        
        for i in 1...3 {
            let teacher = Teacher(firstName: "User", lastName: "\(i)", teachTime: 10)
            teacher.add(className: "Mathematics")
            teacher.add(className: "Science")
        }
    }
}
