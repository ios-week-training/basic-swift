//
//  Day2.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation


class Day2: ObservableObject, RunableProtocol {
    var boolVar1: Bool = false
    var doubleVar: Double = 10.5
    var intVar: Int = 10
    
    let firstName = "Iqbal"
    let lastName = "Rahman"
    
    func execute() {
        // boolean and double
        print("Boolean false : \(boolVar1)")
        print("Double value : \(doubleVar)")
        
        // string interpolation
        print("\(firstName) \(lastName)")
        
        print("- Int and Int -")
        print(intVar + intVar)
        print(intVar - intVar)
        print(intVar * intVar)
        print(intVar / intVar)
        
        print("- Double and Double")
        print(doubleVar + doubleVar)
        print(doubleVar - doubleVar)
        print(doubleVar * doubleVar)
        print(doubleVar / doubleVar)
        
        print("- Int and Double -")
        print(doubleVar + Double(intVar))
        print(doubleVar - Double(intVar))
        print(doubleVar * Double(intVar))
        print(doubleVar / Double(intVar))
    }
}
