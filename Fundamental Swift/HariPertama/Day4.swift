//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day4: ObservableObject, RunableProtocol {
    let surname: String = "Lasso"
    var score: Int = 9
    var scores: Double = 9.8
    
    var books: Set<String> = Set(["The Bluest Eye", "Foundation", "Girl, Woman, Other"])
    var numbers = [1, 2, 3, 4, 5]
    var number: Set = [1, 2, 3, 4, 5]

    func execute() {
        print("Type Annotation")
        print("String : ", surname)
        print("Int Score : ", score)
        print("Double score : ", scores)
        
        for x in books {
            print(x)
        }
        
        print(numbers.count)
        numbers.append(6)
        print("Array :", numbers)
        print("Arr contains 1 :", numbers.contains(1))
        print("Arr contains 7 :", numbers.contains(7))
        
        print(number.count)
        number.insert(6)
        print("Set :", number)
        print("Set contains 1 :", numbers.contains(1))
        print("Set contains 7 :", numbers.contains(7))
        
        
        
    }
}
