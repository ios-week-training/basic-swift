//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day11: ObservableObject, RunableProtocol {
    struct People {
        private var surname: String
        private var age: Int
        fileprivate static let year: Int = 2024
        
        init(surname: String, age: Int) {
            self.surname = surname
            self.age = age
        }
        
        func toString() {
            print("Surname :",surname, age)
        }
        
        static func seeYear() {
            print(year)
        }
    }
    
    func execute() {
        let people1 = People(surname: "Budiman Yahya", age: 21)
        people1.toString()
    }
}
