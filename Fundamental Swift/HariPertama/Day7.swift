//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day7: ObservableObject, RunableProtocol {
    let firstName = "Iqbal"
    let lastName = "Rahman"
    
    var subjectName = ("Iqbal", "Rahman")
    var alphabets = ("A", "B", "C", ("a", "b", "c"))
    var company = (product: "Programiz App", version: 2.1)
    
    func showFullName() {
        print("\(firstName) \(lastName)")
    }
    
    func showSingleName(name: String) {
        print("\(name)")
    }
    
    func returnFullName(text: String) -> String {
        return ("\(text) \(firstName) \(lastName)")
    }
    
    func showText(_ text: String) {
        print("\(text)")
    }
    
    var conditionValue: (Bool) -> String = { condition in
        if condition {
            return "True Condition"
        } else {
            return "False Condition"
        }
    }
    
    func execute() {
        showFullName()
        showSingleName(name: "Budiman")
        
        print(returnFullName(text: "Fullname :"))
        
        print("Tuples : ", subjectName.0, subjectName.1)
        print("Nested Tuples : ", alphabets.3.0)
        print("Named Tuples : ", company.product, company.version)
        
        showText("This is Text")
        
        print(conditionValue(true))
    }
}
