//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day6: ObservableObject, RunableProtocol {
    let count = 1...5
    let max = 10
    var i = 0
    
    func execute() {
        print("Countup")
        for i in count {
            print(i)
        }
        
        print("Countdown")
        for x in stride(from: 5, to: 0, by: -1) {
            print(x)
        }
        
        print("While loops")
        while i < max {
            print(i)
            i += 2
        }
        
        print("Breaks when reach 4 loops")
        i = 0
        while i < max {
            print(i)
            if i == 4 {
                break;
            }
            i += 2
        }
        
        for i in 1...6 {
            for x in i...6 {
                if x == 6 {
                    break;
                }
                print("\(x), ", terminator: "")
            } 
            if i == 5 {
                continue
            }
            print()
        }
    }
}
