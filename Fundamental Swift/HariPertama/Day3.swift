//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day3: ObservableObject, RunableProtocol {
    var dataList = [11, 10, 11]
    let arrayInt: [Int] = [1,2,3,4]
    let dataArr = Set(["b", "a"])
    var dataArrs = [String]()
    
    var dicArr = [1: "a", 2: "b"]
    
    enum Planet {
        case mercury, venus, earth, mars, jupiter, saturn, uranus, neptune
    }
    
    func execute() {
        dataList.append(100)
        print("Var Array data : ", dataList)
        print("Let Array data : ", arrayInt)
        
        print("Dictionary : ", dicArr[1]!)
        print("Tenary dictionary", dicArr[3] ?? "tidak ada")
        
        print("Set :", dataArr.first ?? "kosong")
        
        print("Enumeration : ", Planet.earth)
    }
}
