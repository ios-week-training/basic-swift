//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation
import SwiftUI

class Day5: ObservableObject, RunableProtocol {
    let isPass = true
    
    let a = 10
    let b = 20
    let x = 15
    
    let y = 15
    
    enum Planet: CaseIterable {
        case Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune
    }
    
    func execute() {
        print("IF Statement")
        if isPass {
            print("Success ...")
        }
        
        if x == a {
            print("not Equal ...")
        }
        
        print("Conditional IF")
        if x > a && x < b {
            print("Between :", x)
        } else if x < a || x > b {
            print("Not between")
        }
        
        print("Switch Case")
        let somePlanet = Planet.allCases.randomElement()
        
        switch somePlanet {
            case .Mercury:
                print("Mercur")
            case .Venus:
                print("Venu")
            case .Earth:
                print("Eart")
            case .Mars:
                print("Mar")
            case .Jupiter:
                print("Jupiter")
            case .Saturn:
                print("Saturn")
            case .Uranus:
                print("Uranu")
            case .Neptune:
                print("Neptune")
            default:
                print("Empty ...")
        }
        
        print("Tenary operator")
        print(y == x ? "Equals ..." : "Not equals ...")
    }
}
