//
//  RunableProtocol.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

protocol RunableProtocol {
    func execute()
}
