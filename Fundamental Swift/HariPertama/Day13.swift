//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

protocol Vehicle {
    var name: String { get }
    var currentPassengers: Int { get set }
    func estimateTime(for distance: Int) -> Int
    func travel(distance: Int)
}

protocol VehicleSound {
    func sound() -> String
}

extension String {
    func trimmed() -> String {
        self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    mutating func trim() {
        self = self.trimmed()
    }
}

extension VehicleSound {
    func sound() -> String {
        return "Ngeng..."
    }
}

class Day13: ObservableObject, RunableProtocol {
    struct Bicycle: Vehicle, VehicleSound {
        var name: String
        var currentPassengers: Int
        
        func estimateTime(for distance: Int) -> Int {
            distance / 10
        }

        func travel(distance: Int) {
            print("I'm cycling \(distance)km.")
        }
        
        func sound() -> String {
            return "Kring..."
        }
    }
    
    struct Motor: VehicleSound {
    }
    
    func getSound() -> some VehicleSound {
        return Motor()
    }
    
    func trimmed(_ string: String) -> String {
        string.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func execute() {
        let cycle = Bicycle(name: "Honda Beat", currentPassengers: 2)
        cycle.travel(distance: cycle.estimateTime(for: 110))
        
        let vehicle = getSound()
        print(vehicle.sound())
        
        var text = "    Some text with  space to much  "
        print("old method :",trimmed(text))
        print(text)
        text.trim()
        print("new method :",text)
    }
}
