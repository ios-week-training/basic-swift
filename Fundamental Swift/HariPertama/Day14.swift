//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day14: ObservableObject, RunableProtocol {
    var username: String? = nil
    var myVar: Int? = nil
    let input = ""
    let data = [1,2,3]
    
    func printSquare(of number: Int?) {
        guard let number = number else {
            print("Missing input")
            return
        }
        print("\(number)")
    }
    
    func getData(id: Int) throws -> String {
        return "Not found ..."
    }
    
    struct Person {
        let firstName: String
        let lastName: String?
    }
    
    func execute() {
        if let name = username {
            print("Name : \(name)")
        } else {
            print("The optional was empty.")
        }
        
        printSquare(of: myVar)
        
        let number = Int(input) ?? 0
        print(number)

        let person: Person? = nil
        let lastName = person?.lastName?.first?.uppercased() ?? "Empty"
        print(lastName)
        
        let findData = (try? getData(id: 3)) ?? "Not Found"
        print(findData)
    }
}
