//
//  day1.swift
//  Fundamental Swift
//
//  Created by Adjie Satryo Pamungkas on 04/03/24.
//

import Foundation

class Day1: ObservableObject, RunableProtocol {
    
    let tesInt: Int = 0
    let tesDouble: Double = 0.0
    let tesString: String = ""
    
    var tesVarInt: Int = 0
    var tesVarDouble: Double = 0.0
    var tesVarString: String = "Hello"
    
    func execute() {
        
        let newInt = tesInt + 1
        print("Int:")
        print(tesInt)
        print(newInt)
        let newDouble = tesDouble + 1
        print("Double:")
        print(tesDouble)
        print(newDouble)
        let newString = tesString + "World"
        print("String:")
        print(tesString)
        print(newString)
        print("Full String:")
        print("\(newInt) + \(newDouble) + \(newString)")

        tesVarInt += 1
        print("Int:")
        print(tesVarInt)
        tesVarDouble += 1
        print("Double:")
        print(tesVarDouble)
        tesVarString += "World"
        print("String:")
        print(tesVarString)
        print("Full String:")
        print("\(tesVarInt) + \(tesVarDouble) + \(tesVarString)")
    }
}
