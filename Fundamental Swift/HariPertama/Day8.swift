//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day8: ObservableObject, RunableProtocol {
    enum errorMessage: Error {
        case invalidInput
        case errorProcess
    }
    
    func doSomething(isPass: Bool = true) {
        if isPass {
            print("- Success ...")
        } else {
            print("- failed ...")
        }
    }
    
    func processPlay(name: String, flows: Bool = false) throws {
        if flows {
            throw errorMessage.invalidInput
        } else {
            print("Hello \(name) ...")
        }
    }
    
    func execute() {
        print("Default params : ")
        doSomething()
        print("With params : ")
        doSomething(isPass: false)
        
        do {
            try processPlay(name: "Iqbal\n", flows: true)
        } catch errorMessage.errorProcess {
            print("Error processing ...")
        } catch {
            print("Unexpected error")
        }
        
        do {
            try processPlay(name: "Iqbal")
        } catch _ {
            print(errorMessage.errorProcess)
        }
    }
}
