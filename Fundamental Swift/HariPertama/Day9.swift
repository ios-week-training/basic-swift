//
//  day3.swift
//  Fundamental Swift
//
//  Created by Iqbal Rahman on 04/03/24.
//

import Foundation

class Day9: ObservableObject, RunableProtocol {
    let team = ["Gloria", "Suzanne", "Piper", "Tiffany", "Tasha"]
    
    var sayHello = {
        print("Hello Iqbal")
    }
    
    var sayHellos = { (text: String) in
        print("hello \(text)")
    }
    
    var sayHelloss = { (text: String) -> String in
        return "hello \(text)"
    }
    
    let playWithCat = {
        print("Meow ...")
    }
    
    func play(using playType: () -> Void) {
        print("Call a cat")
        playType()
    }
    
    func classRoom(name: String, lesson: () -> Void) {
        print("Welcome to \(name)!")
        lesson()
    }
    
    func execute() {
        print("- Basic Closure -")
        sayHello()
        
        print("- Parameter Closure -")
        sayHellos("Iqbal Rahman")
        
        print("- Return Closure -")
        print(sayHelloss("Budiman"))
        
        let captainFirstTeam = team.sorted { name1, name2 in
            if name1 == "Suzanne" {
                return true
            }

            return name1 < name2
        }
        
        print(captainFirstTeam)
        
        play(using: playWithCat)
        
        classRoom(name: "Iqbal") {
            print("Enter classroom")
            print("Study swift ...")
        }
        
    }
}
