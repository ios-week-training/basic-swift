////
////  ContentView.swift
////  Fundamental Swift
////
////  Created by Adjie Satryo Pamungkas on 04/03/24.
////
//
//import SwiftUI
//
//struct ContentView: View {
//    
//    @StateObject private var presenter = PersonManager()
//    
//    var body: some View {
//        VStack {
//            Image(systemName: "globe")
//                .imageScale(.large)
//                .foregroundStyle(.tint)
//            Text("Hello, world!")
//        }
//        .padding()
//        .onAppear {
//            presenter.execute()
//        }
//    }
//}
//
//#Preview {
//    ContentView()
//}
