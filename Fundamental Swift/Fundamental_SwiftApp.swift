//
//  Fundamental_SwiftApp.swift
//  Fundamental Swift
//
//  Created by Adjie Satryo Pamungkas on 04/03/24.
//

import SwiftUI

@main
struct Fundamental_SwiftApp: App {
    var body: some Scene {
        WindowGroup {
            ContactScreen()
        }
    }
}
